﻿package com.example.InvoiceRestApi.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;

import com.example.InvoiceRestApi.Model.InvoiceItem;

@Service
public class InvoiceService {

    public List<InvoiceItem> createInvoice() {
        ArrayList<InvoiceItem> invoiceList = new ArrayList<>();
        InvoiceItem invoice1 = new InvoiceItem(1, "sofa", 2, 12000.00);
        InvoiceItem invoice2 = new InvoiceItem(2, "chair", 3, 22000.00);
        InvoiceItem invoice3 = new InvoiceItem(3, "bed", 5, 52000.00);
        invoiceList.addAll(Arrays.asList(invoice1, invoice2, invoice3));

        return invoiceList;
    }

}
