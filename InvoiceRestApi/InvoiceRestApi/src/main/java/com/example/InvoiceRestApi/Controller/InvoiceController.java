﻿package com.example.InvoiceRestApi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.InvoiceRestApi.Model.InvoiceItem;
import com.example.InvoiceRestApi.Service.InvoiceService;

@RestController

@RequestMapping("/api")
@CrossOrigin
public class InvoiceController {

    @Autowired
    private InvoiceService service;

    @GetMapping("/invoice")

    public List<InvoiceItem> getInvoice() {
        return service.createInvoice();
    }

}
